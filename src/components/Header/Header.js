import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import './Header.scss'

export const Header = ({ username }) => {
  const getName = () => {
    return username || 'random citizen'
  }
  return (
    <nav className="navbar navbar-default navbar-mini">
      <div className="container">
        <div className="navbar-header flex">
          <div className="text-left">
            <span>Hello, {getName()}</span>
          </div>
          <div className="text-right">
            Check out our <a href="#">github</a> profile and collaborate with us!
          </div>
        </div>
      </div>
    </nav>
  )
}

Header.propTypes = {
  username: PropTypes.string.isRequired
}

const mapStateToProps = (state) => {
  return state.user
}
export default connect(mapStateToProps)(Header)
