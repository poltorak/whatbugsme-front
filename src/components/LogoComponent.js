import React from 'react'
import LOGO from '../assets/images/logo.png'

export const Logo = () => (
  <div className="row">
    <div className="col-xs-12">
      <div className="logo">
        <img src={LOGO} alt="Logo" />
      </div>
    </div>
  </div>
)

export default Logo
