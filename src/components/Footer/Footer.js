import React from 'react'
import './Footer.scss'

export const Footer = () => (
  <footer className="footer">
    <div className="container">
      <div className="row">
        <div className="col-sm-6 col-xs-12">
          <span>
            This project is part of <strong>RST Softwarehouse</strong> Open Source something. <br /> RST Softwarehouse is a part of RST Group.
          </span>
        </div>
        <div className="col-sm-6 col-xs-12">
          Check out our <a href="#">github</a> profile and collaborate with us!
        </div>
      </div>
    </div>
  </footer>
)

export default Footer
