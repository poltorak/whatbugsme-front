import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path : 'bugs-list',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const BugsList = require('./containers/BugsListContainer').default
      const reducer = require('./modules/bugsList').default

      /*  Add the reducer to the store on key 'BugsList'  */
      injectReducer(store, { key: 'bugslist', reducer })

      /*  Return getComponent   */
      cb(null, BugsList)

    /* Webpack named bundle   */
    }, 'bugslit')
  }
})
