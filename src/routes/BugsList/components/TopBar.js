import React from 'react'
import Logo from '../../../components/LogoComponent'
import './TopBar.scss'

export class TopBar extends React.Component {

  render() {
    return (
      <div className="topbar">
        <div className="container">
          <div className="col">
            <Logo />
          </div>
          <div className="col">
            <ul>
              <li>
                <a href="#">Invite new user</a>
              </li>
              <li>
                <a href="#">Log out</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

export default TopBar
