import React from 'react'
import Logo from '../../../components/LogoComponent'
import AccountWrapper from './Account/AccountWrapper'
import InfoAbout from './InfoAbout'
import { Link } from 'react-router'
import './HomeView.scss'

export const HomeView = (props) => (
  <section className="branding">
    <div className="container">
      <Logo />
      <Link to={'/bugs-list'}>List</Link>
      <div className="row main-content">
        <div className="col-xs-12 col-sm-6 col-sm-offset-1">
          <h1>Dont be afraid to tell others <br /><strong>what bugs you</strong> about your workplace! </h1>
          <small>(It’s all private now)</small>
        </div>
        <div className="col-xs-12 col-sm-5">
          <AccountWrapper />
        </div>
      </div>
      <div className="row learn-more">
        <div className="col-xs-12">
          <a href="#infos" className="btn-ghost">
            Learn more
          </a>
        </div>
      </div>
    </div>
    <InfoAbout />
  </section>
)

export default HomeView
