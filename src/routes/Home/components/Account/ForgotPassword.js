import React, { PropTypes } from 'react'

class ForgotPassword extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      email: '',
      emailConfirm: ''
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange () {
    this.setState({
      email: this.refs.email.value,
      emailConfirm: this.refs.password.value
    })
  }

  handleSubmit (event) {
    event.preventDefault()
    console.log('forgot password submit', this.state)
  }
  render () {
    return (
      <form className="account-form" onSubmit={this.handleSubmit}>
        <h3>Password <span className="highlight">recovery</span></h3>
        <div className="input-field">
          <label htmlFor="email">Your e-mail address</label>
          <input
            type="email"
            id="email"
            name="email"
            placeholder="Write your email here"
            required
            ref="email"
            value={this.state.username}
            onChange={this.handleChange}
          />
        </div>
        <div className="input-field">
          <label htmlFor="emailConfirm">Repeat your e-mail address</label>
          <input
            type="email"
            id="emailConfirm"
            name="emailConfirm"
            placeholder="Repeat your email"
            required
            ref="emailConfirm"
            value={this.state.emailConfirm}
            onChange={this.handleChange}
          />
        </div>
        <div className="form-actions">
          <button className="primary-btn" type="submit">Send</button>
          <button className="secondary-btn" type="button" onClick={this.props.backToLogin}>Go back to login</button>
        </div>
      </form>
    )
  }
}

ForgotPassword.propTypes = {
  backToLogin: PropTypes.func.isRequired
}

export default ForgotPassword
