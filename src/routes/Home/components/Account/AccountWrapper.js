import React from 'react'
import Login from './Login'
import Register from './Register'
import ForgotPassword from './ForgotPassword'
import './Account.scss'

class AccountWrapper extends React.Component {
  constructor () {
    super(...arguments)
    this.state = {
      activeTabName: 'login'
    }
    this.tabClass = this.tabClass.bind(this)
    this.activateTab = this.activateTab.bind(this)
  }

  tabClass (tabName) {
    if (tabName instanceof Array) {
      return ~tabName.indexOf(this.state.activeTabName) ? 'active' : ''
    }
    return tabName === this.state.activeTabName ? 'active' : ''
  }

  activateTab (event, tabName) {
    event.preventDefault()
    this.setState({
      activeTabName: tabName
    })
  }

  getTabComponent() {
    switch (this.state.activeTabName) {
      case 'login':
        return <Login forgotPasswordClick={(event) => this.activateTab(event, 'forgotPassword')} />
      case 'register':
        return <Register />
      case 'forgotPassword':
        return <ForgotPassword backToLogin={(event) => this.activateTab(event, 'login')} />
      default:
        return <Login />
    }
  }

  render () {
    return (
      <div className="tab-wrapper">
        <div className="tabs">
          <a
            href="#"
            ref="register"
            className={`tab ${this.tabClass('register')}`}
            onClick={(event) => this.activateTab(event, 'register')}
          >Register</a>
          <a
            href="#"
            ref="login"
            className={`tab ${this.tabClass(['login', 'forgotPassword'])}`}
            onClick={(event) => this.activateTab(event, 'login')}
          >Login</a>
        </div>
        <div className="tabs-content">
          {this.getTabComponent()}
        </div>
      </div>
    )
  }
}

export default AccountWrapper
