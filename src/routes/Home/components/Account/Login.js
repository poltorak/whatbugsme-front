import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { login } from '../../reducer'

class Login extends React.Component {
  constructor (props) {
    super(props)
    let { username, onSubmit } = this.props
    this.state = {
      username: username || 'asd@asd.com',
      password: 'password'
    }
    this.onSubmit = onSubmit
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange () {
    this.setState({
      username: this.refs.email.value,
      password: this.refs.password.value
    })
  }

  handleSubmit (event) {
    event.preventDefault()
    this.onSubmit(this.state)
  }
  render () {
    return (
      <form className="account-form" onSubmit={this.handleSubmit}>
        <h3>Log in to your <span className="highlight">account</span></h3>
        <div className="input-field">
          <label htmlFor="email">Your e-mail address</label>
          <input
            type="email"
            id="email"
            name="email"
            placeholder="Write your email here"
            required
            ref="email"
            value={this.state.username}
            onChange={this.handleChange}
          />
        </div>
        <div className="input-field">
          <label htmlFor="password">Your password</label>
          <input
            type="password"
            id="password"
            name="password"
            placeholder="Put your password"
            required
            ref="password"
            value={this.state.password}
            onChange={this.handleChange}
          />
        </div>
        <div className="form-actions">
          <button className="primary-btn" type="submit">Log in</button>
          <button className="secondary-btn" type="button" onClick={this.props.forgotPasswordClick}>Forgot password?</button>
        </div>
      </form>
    )
  }
}

Login.propTypes = {
  username: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  forgotPasswordClick: PropTypes.func.isRequired
}

const mapDispatchToProps = (dispatch) => {
  return {
    onSubmit: (data) => {
      dispatch(login(data))
    }
  }
}

const mapStateToProps = (state) => {
  return state.user
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
