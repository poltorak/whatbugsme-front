import React from 'react'

export default class Register extends React.Component {
  constructor() {
    super()

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)

    this.state = {
      username: '',
      password: '',
      passwordConfirm: ''
    }
  }

  handleChange () {
    this.setState({
      username: this.refs.email.value,
      password: this.refs.password.value,
      passwordConfirm: this.refs.passwordConfirm.value
    })
  }

  handleSubmit(event) {
    event.preventDefault()
    console.log('register submit')
  }

  render() {
    return (
      <form className="account-form" onSubmit={this.handleSubmit}>
        <h3>Create your <span className="highlight">free account</span>!</h3>
        <div className="input-field">
          <label htmlFor="email">Your e-mail address</label>
          <input ref="email" type="email" id="email" name="email" value={this.state.username} placeholder="Write your email here" required onChange={this.handleChange} />
        </div>
        <div className="flex">
          <div className="input-field">
            <label htmlFor="password">Your password</label>
            <input
              type="password"
              name="password"
              placeholder="Put your password"
              required
              ref="password"
              id="password"
              value={this.state.password}
              onChange={this.handleChange}
            />
          </div>
          <div className="input-field">
            <label htmlFor="passwordConfirm">Repeat password</label>
            <input
              id="passwordConfirm"
              type="password"
              name="passwordConfirm"
              required
              ref="passwordConfirm"
              value={this.state.passwordConfirm}
              placeholder="Repeat password"
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div className="form-actions">
          <button className="primary-btn" type="submit">Register now!</button>
        </div>
      </form>
    )
  }
}
