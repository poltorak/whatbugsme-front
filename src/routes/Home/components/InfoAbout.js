import React from 'react'

const InfoAbout = () => (
  <div className="infos" id="infos">
    <div className="container">
      <div className="row">
        <div className="col-xs-12 col-sm-4">
          <h4>Anonymous</h4>
          <p>
            What bugs me is a Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          </p>
        </div>
        <div className="col-xs-12 col-sm-4">
          <h4>Vote for the entries </h4>
          <p>What bugs me is a Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        </div>
        <div className="col-xs-12 col-sm-4">
          <h4>Open Source</h4>
          <p>
          What bugs me is a Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          </p>
        </div>
      </div>
    </div>
  </div>
)

export default InfoAbout
