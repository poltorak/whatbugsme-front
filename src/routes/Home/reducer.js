export const LOGIN = 'LOGIN'

export function login({ username, password }) {
  return {
    type: LOGIN,
    payload: { username, password }
  }
}

const initialState = { username: '', password: '' }
export default function loginReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        username: action.payload.username
      }
    default:
      return state
  }
}
